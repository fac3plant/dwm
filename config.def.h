/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int tagpad    = 5;        /* padding for tags */
static const unsigned int gappx     = 20;        /* Gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;        /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "lucidasanstypewriter-10:size=10" };
static const char dmenufont[]       = "lucidasanstypewriter-10:size=10";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_prpl[]        = "#990099";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray4, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray2, col_gray1,  col_prpl },
    [SchemeOcc]  = { col_prpl, col_gray1, col_gray2 },
};

/* tagging */
static const char *tags[] = {
    "home",         /* Workspace 1 */
    "files",        /* Workspace 2 */
    "office",       /* Workspace 3 */
    "browser",      /* Workspace 4 */
    "videos",       /* Workspace 5 */
    "other",        /* Workspace 6 */
    "games",        /* Workspace 7 */
    "social",       /* Workspace 8 */
    "coding",       /* Workspace 9 */
};

static const unsigned int ulinepad      = 5;    /* horizontal padding between the underline and tag */
static const unsigned int ulinestroke   = 2;    /* thickness / height of the underline */
static const unsigned int ulinevoffset  = 0;    /* how far above the bottom of the bar the line should appear */
static const int ulineall               = 0;    /* 1 to show underline on all tags, 0 for just the active ones */

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class        instance    title       tags mask       isfloating      monitor */
    { "float",      NULL,       NULL,       0,              True,           -1 },
    { "calc",       NULL,       NULL,       0,              True,           -1 },
    { "moc",        NULL,       NULL,       1 << 0,         False,          -1 },
    { "Pale moon",  NULL,       NULL,       1 << 3,         False,          -1 },
    { "firefox",    NULL,       NULL,       1 << 3,         False,          -1 },
    { "Steam",      NULL,       NULL,       1 << 6,         False,           0 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[T]",      tile },    /* first entry is default */
	{ "[F]",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define ALTKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *termcmd[]  = { "st", NULL };

static const char *lock[] = { "slock", NULL };
static const char *rcrd_screencast[] = { "screencast", NULL };
static const char *kill_screencast[] = { "killall", "ffmpeg", NULL };
static const char *scrot[]           = { "scrot", "-z", NULL };
/* terminal stuff */
static const char *file[] = {"st", "-e", "lf", NULL };
static const char *musc[] = {"st", "-e", "mocp", NULL };
static const char *math[] = {"st", "-e", "calc", NULL };
static const char *flot[] = {"st", "-c", "float", NULL };
/* music and sound controls */
static const char *pase[] = {"mocp", "--toggle-pause", NULL};
static const char *next[] = {"mocp", "--next", NULL};
static const char *prev[] = {"mocp", "--previous", NULL};
static const char *mute[] = {"amixer", "-q", "set", "Master", "toggle", NULL};
static const char *volu[] = {"amixer", "-q", "set", "Master", "5%+", "unmute", NULL};
static const char *vold[] = {"amixer", "-q", "set", "Master", "5%-", "unmute", NULL};
/* application launchers */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_prpl, "-sf", col_gray4, NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */
    { MODKEY,                       XK_bracketleft, spawn,     {.v = vold } },
    { MODKEY,                       XK_bracketright, spawn,    {.v = volu } },
    { MODKEY,                       XK_backslash, spawn,       {.v = mute } },
    { 0,                            0x1008ff11, spawn,         {.v = vold } },
    { 0,                            0x1008ff12, spawn,         {.v = mute } },
    { 0,                            0x1008ff13, spawn,         {.v = volu } },
    { MODKEY,                       XK_F6,     spawn,          {.v = rcrd_screencast } },
    { MODKEY|ShiftMask,             XK_F6,     spawn,          {.v = kill_screencast } },
    { 0,                            XK_Print,  spawn,          {.v = scrot } },
    { ALTKEY|ControlMask,           XK_l,      spawn,          {.v = lock } },
	{ MODKEY,                       XK_space,  spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
    { MODKEY|ShiftMask,             XK_Return, spawn,          {.v = flot } },
    { MODKEY,                       XK_r,      spawn,          {.v = file } },
    { MODKEY,                       XK_c,      spawn,          {.v = math } },
    { MODKEY|ControlMask,           XK_0,      spawn,          {.v = musc } },
    { ALTKEY,                       XK_p,      spawn,          {.v = pase } },
    { ALTKEY,                       XK_b,      spawn,          {.v = prev } },
    { ALTKEY,                       XK_n,      spawn,          {.v = next } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ ALTKEY,                       XK_Return, zoom,           {0} },
	{ ALTKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_q,      killclient,     {0} },
    { MODKEY|ShiftMask,             XK_f,      togglefullscr,  {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ ALTKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ ALTKEY,                       XK_space,  setlayout,      {0} },
	{ ALTKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_e,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

